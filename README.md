# go-warpwire

[![pipeline status](https://gitlab.oit.duke.edu/devil-ops/go-warpwire/badges/main/pipeline.svg)](https://gitlab.oit.duke.edu/devil-ops/go-warpwire/-/commits/main)
[![coverage report](https://gitlab.oit.duke.edu/devil-ops/go-warpwire/badges/main/coverage.svg)](https://gitlab.oit.duke.edu/devil-ops/go-warpwire/-/commits/main)
[![Latest Release](https://gitlab.oit.duke.edu/devil-ops/go-warpwire/-/badges/release.svg)](https://gitlab.oit.duke.edu/devil-ops/go-warpwire/-/releases)

Go library and command line client for interacting with the WarpWire API

## WarpwireCTL

Command line utility for interacting with WarpWire API. Installation can be done
either by directly downloading the appropriate binaries from the
[release](https://gitlab.oit.duke.edu/devil-ops/go-warpwire/-/releases) page, or
you can use homebrew/yum after setting up the devil-ops repository.
Instructions on setting this up are
[here](https://gitlab.oit.duke.edu/devil-ops/installing-devil-ops-packages). The
package name is `warpwirectl`.

For the most up-to-date documentation, use `warpwirectl --help`.

### Authentication

Currently you'll need a WarpWire guest account with appropriate permissions to access the API. Once you have that, set the username and password as environment variables:

```bash
❯ export WARPWIRE_USERNAME=my-username
❯ export WARPWIRE_PASSWORD=my-password
```

### View and Play Statistics

You can see view/play statistics using the `stats` subcommand. Just provide the
permalink for a given library or asset. You can find the permalink by clicking
the `share` button inside of WarpWire.

Example:

```bash
❯ warpwirectl stats https://warpwire.duke.edu/w/a5r2s/
Library: My Awesome Podcast
Episode: My Awesome Podcast S01EP00
2023-02-16 - 1 - 6m18s
2023-03-02 - 1 - 30m1s
2023-03-06 - 7 - 13m22s
2023-03-07 - 1 - 4m2s
2023-03-08 - 6 - 34m52s
Episode: My Awesome Podcast S01EP01
2023-03-06 - 3 - 53s
2023-03-13 - 2 - 2s
```
