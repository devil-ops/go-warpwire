package main

import (
	"context"
	"fmt"
	"log"
	"os"

	warpwire "gitlab.oit.duke.edu/devil-ops/warpwire-reports/pkg"
)

func main() {
	if len(os.Args) != 2 {
		log.Fatalf("Usage: %v ID", os.Args[0])
	}
	c := warpwire.New(warpwire.WithAuthEnv())
	items, err := c.HotspotService.UsageWithLibrary(context.TODO(), os.Args[1])
	if err != nil {
		panic(err)
	}
	fmt.Printf("%+v\n", items)
}
