package main

import (
	"fmt"
	"os"

	warpwire "gitlab.oit.duke.edu/devil-ops/go-warpwire/pkg"
)

func main() {
	if len(os.Args) != 3 {
		panic("Usage: ./ username password")
	}

	c := warpwire.New(
		warpwire.WithUsername(os.Args[1]),
		warpwire.WithPassword(os.Args[2]),
	)
	t, err := c.GetToken()
	if err != nil {
		panic(err)
	}
	fmt.Print(t)
}
