package main

import "gitlab.oit.duke.edu/devil-ops/go-warpwire/cmd/warpwirectl/cmd"

func main() {
	cmd.Execute()
}
