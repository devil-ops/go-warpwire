package cmd

import (
	"fmt"
	"sort"
	"time"

	"github.com/spf13/cobra"
	warpwire "gitlab.oit.duke.edu/devil-ops/go-warpwire/pkg"
)

// statsCmd represents the stats command
var statsCmd = &cobra.Command{
	Use:     "stats",
	Short:   "Statistics for a given piece of WarpWire",
	Aliases: []string{"statistics", "s"},
	Args:    cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		includeUnpub, err := cmd.Flags().GetBool("include-unpublished")
		cobra.CheckErr(err)
		includeUnviewed, err := cmd.Flags().GetBool("include-unviewed")
		cobra.CheckErr(err)
		startDaysAgo, err := cmd.Flags().GetInt("start-days-ago")
		cobra.CheckErr(err)

		now := time.Now()
		startAgo := time.Hour * 24 * time.Duration(startDaysAgo)
		start := now.Add(-startAgo)

		uuids, err := client.Link.UUID(ctx, args[0])
		cobra.CheckErr(err)

		// If no asset id is returned from the uuids, we use the Media Library UUID instead
		var aid string
		var singleAsset bool
		if uuids.Properties.AssetUUID == "" {
			aid = uuids.Properties.MediaLibraryUUID
		} else {
			aid = uuids.Properties.AssetUUID
			singleAsset = true
		}

		catalog, err := client.Data.Catalog(ctx, uuids.Properties.MediaLibraryUUID, aid)
		cobra.CheckErr(err)
		// Sorty by date
		sort.Slice(catalog.List, func(i, j int) bool {
			return catalog.List[i].Created < catalog.List[j].Created
		})

		description, err := client.Data.Describe(ctx, uuids.Properties.MediaLibraryUUID, uuids.Properties.MediaLibraryUUID)
		cobra.CheckErr(err)

		fmt.Printf("Library: %v\nFormat: DAY - UNIQUE_USERS\n", description.Title)

		if singleAsset {
			assetDesc, err := client.Data.Describe(ctx, uuids.Properties.MediaLibraryUUID, aid)
			cobra.CheckErr(err)

			fmt.Printf("Episode: %v\n", assetDesc.Title)
			stats, err := client.Reporting.Views(ctx, warpwire.ReportingOpts{
				Collection: uuids.Properties.MediaLibraryUUID,
				Asset:      aid,
				Start:      &start,
			})
			cobra.CheckErr(err)

			for _, stat := range stats.Stats {
				if (stat.Seconds != 0 && stat.Viewers != 0) || includeUnviewed {
					fmt.Printf("%v - %v\n", stat.Day.Format("2006-01-02"), stat.Viewers)
				}
			}
		} else {
			for _, asset := range catalog.List {
				assetDesc, err := client.Data.Describe(ctx, uuids.Properties.MediaLibraryUUID, asset.UUID)
				cobra.CheckErr(err)

				if assetDesc.Publish || includeUnpub {
					fmt.Printf("Episode: %v\n", assetDesc.Title)
					stats, err := client.Reporting.Views(ctx, warpwire.ReportingOpts{
						Collection: uuids.Properties.MediaLibraryUUID,
						Asset:      asset.UUID,
						Start:      &start,
					})
					cobra.CheckErr(err)

					for _, stat := range stats.Stats {
						if (stat.Seconds != 0 && stat.Viewers != 0) || includeUnviewed {
							fmt.Printf("%v - %v\n", stat.Day.Format("2006-01-02"), stat.Viewers)
						}
					}
				}
			}
		}
	},
}

func init() {
	rootCmd.AddCommand(statsCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// statsCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	statsCmd.Flags().Bool("include-unpublished", false, "Include un-published episodes")
	statsCmd.Flags().Bool("include-unviewed", false, "Include days where there were 0 viewers and seconds viewed")
	statsCmd.Flags().Int("start-days-ago", 30, "Number of days before now to reach back for statistics")
}
