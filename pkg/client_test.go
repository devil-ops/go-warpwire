package warpwire

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

var (
	ctx    = context.TODO()
	client *Client
	srv    *httptest.Server
)

func TestMain(m *testing.M) {
	setup()
	code := m.Run()
	shutdown()
	os.Exit(code)
}

func setup() {
	srv = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		switch {
		case strings.HasSuffix(r.URL.Path, "/api/usage/activity/code/library-id"):
			testCopyFile("./testdata/usage_activity.json", w)
			return
		case strings.HasPrefix(r.URL.Path, "/api/link/code/"):
			testCopyFile("./testdata/link/asset.json", w)
			return
		case strings.HasPrefix(r.URL.Path, "/api/stats/views/c/library-id/o/asset-id/"):
			testCopyFile("./testdata/stats_views.json", w)
			return
		case strings.HasPrefix(r.URL.Path, "/api/stats/plays/c/library-id/o/asset-id/"):
			testCopyFile("./testdata/stats_views.json", w)
			return
		case strings.HasPrefix(r.URL.Path, "/api/catalog/c/some-collection/o/some-asset"):
			testCopyFile("./testdata/data/catalog.json", w)
			return
		case strings.HasPrefix(r.URL.Path, "/api/describe/c/some-collection/o/some-asset"):
			testCopyFile("./testdata/data/describe.json", w)
			return
		default:
			panic(fmt.Sprintf("Unexpected location: %v", r.URL.String()))
		}
	}))
	client = New(WithBaseURL(srv.URL), WithPassword("some-password"), WithUsername("some-username"), WithToken("some-token"))
}

func shutdown() {
	srv.Close()
}

func TestNewClient(t *testing.T) {
	got := New(WithUsername("foo"), WithPassword("bar"))
	require.NotNil(t, got)
}

func TestNewClientErr(t *testing.T) {
	require.PanicsWithValue(t, "must set username", func() { New() })
	require.PanicsWithValue(t, "must set password", func() { New(WithUsername("foo")) })
}

func TestAuthUnmarshal(t *testing.T) {
	tests := map[string]struct {
		auth string
	}{
		"int-date": {
			auth: `{"status":"success","token":"some-token","method":"x-auth-wwtoken","expiration":1678717955}`,
		},
		"string-date": {
			auth: `{"status":"success","token":"some-token","method":"x-auth-wwtoken","expiration":"1678717955"}`,
		},
	}
	for desc, tt := range tests {
		var got AuthenticationResponse
		err := json.Unmarshal([]byte(tt.auth), &got)
		require.NoError(t, err, desc)
	}
}

func testCopyFile(f string, w io.Writer) {
	rp, err := os.ReadFile(f)
	panicIfErr(err)
	_, err = io.Copy(w, bytes.NewReader(rp))
	panicIfErr(err)
}
