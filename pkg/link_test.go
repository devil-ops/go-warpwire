package warpwire

import (
	"context"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestLinkCode(t *testing.T) {
	got, err := client.Link.UUID(context.TODO(), "warpwire.duke.edu/w/asset-id")
	require.NoError(t, err)
	require.NotNil(t, got)
}
