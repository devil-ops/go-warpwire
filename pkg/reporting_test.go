package warpwire

import (
	"context"
	"testing"
	"time"

	"github.com/google/go-querystring/query"
	"github.com/stretchr/testify/require"
)

func TestReportingViews(t *testing.T) {
	items, err := client.Reporting.Views(context.TODO(), ReportingOpts{
		Collection: "library-id",
		Asset:      "asset-id",
	})
	require.NoError(t, err)
	require.NotNil(t, items)
}

func TestReportingPlays(t *testing.T) {
	items, err := client.Reporting.Plays(context.TODO(), ReportingOpts{
		Collection: "library-id",
		Asset:      "asset-id",
	})
	require.NoError(t, err)
	require.NotNil(t, items)
}

func TestReportOptsEncoding(t *testing.T) {
	end := time.Date(2021, time.Month(2), 21, 1, 10, 30, 0, time.UTC)
	got, err := query.Values(ReportingOpts{
		Collection: "foo",
		End:        &end,
	})
	require.NoError(t, err)
	require.Equal(t, "end=2021-02-21", got.Encode())
}
