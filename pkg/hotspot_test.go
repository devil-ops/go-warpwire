package warpwire

import (
	"encoding/json"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestUnmarshalActivityResponse(t *testing.T) {
	b, err := os.ReadFile("testdata/usage_activity.json")
	require.NoError(t, err)

	r := ActivityResponse{}
	err = json.Unmarshal(b, &r)
	require.NoError(t, err)

	require.NotNil(t, r)
	require.Contains(t, r, "5FF6DE56-D2E0-4319-9A8E-9C030D389BC9")
	require.Equal(t, r["5FF6DE56-D2E0-4319-9A8E-9C030D389BC9"].Title, "Super Awesome Podcast S01EP00")
}

func TestHotspotUsageLibrary(t *testing.T) {
	items, err := client.Hotspot.UsageWithLibrary(ctx, "library-id")
	require.NoError(t, err)
	require.NotNil(t, items)
}
