package warpwire

import (
	"context"
	"fmt"
	"net/http"
)

// DataService is the interface for interacting with the Data endpoints
type DataService interface {
	Catalog(context.Context, string, string) (*CatalogResponse, error)
	Describe(context.Context, string, string) (*DescribeResponse, error)
}

// DataServiceOp is the operator for the DataService
type DataServiceOp struct {
	client *Client
}

// DescribeResponse is the response from a describe action
type DescribeResponse struct {
	AudioSourceCount             float64    `json:"audioSourceCount,omitempty"`
	Authenticated                bool       `json:"authenticated,omitempty"`
	CanContribute                string     `json:"canContribute,omitempty"`
	CanEdit                      string     `json:"canEdit,omitempty"`
	CatalogPermLink              string     `json:"catalogPermLink,omitempty"`
	CatalogTitle                 string     `json:"catalogTitle,omitempty"`
	Created                      float64    `json:"created,omitempty"`
	Description                  string     `json:"description,omitempty"`
	Duration                     float64    `json:"duration,omitempty"`
	Hidden                       bool       `json:"hidden,omitempty"`
	Images                       DataImages `json:"images,omitempty"`
	IsAutoCreated                float64    `json:"isAutoCreated,omitempty"`
	MediaProcessingError         string     `json:"mediaProcessingError,omitempty"`
	MediaProcessingStatus        string     `json:"mediaProcessingStatus,omitempty"`
	MediaProcessingTransactionID string     `json:"mediaProcessingTransactionId,omitempty"`
	/*
		This sometimes appears as a map and sometimes a list, just ignoring for now
		Metadata                     struct {
			Description string `json:"description,omitempty"`
		} `json:"metadata,omitempty"`
	*/
	Modified float64 `json:"modified,omitempty"`
	PermLink string  `json:"permLink,omitempty"`
	// Properties          DescribeProperties `json:"properties,omitempty"`
	Public              bool     `json:"public,omitempty"`
	Publish             bool     `json:"publish,omitempty"`
	Scope               string   `json:"scope,omitempty"`
	Title               string   `json:"title,omitempty"`
	Type                string   `json:"type,omitempty"`
	UsageLimitsExceeded bool     `json:"usageLimitsExceeded,omitempty"`
	UserID              string   `json:"userId,omitempty"`
	UserSettings        struct{} `json:"userSettings,omitempty"`
	UUID                string   `json:"uuid,omitempty"`
	VideoSourceCount    float64  `json:"videoSourceCount,omitempty"`
}

/*
Removing this for now. This item mutates depending on what you query, need a better way to handle that
type DescribeProperties struct {
	AssetSharePageLayout     string `json:"assetSharePageLayout,omitempty"`
	EffectiveSharePageLayout string `json:"effectiveSharePageLayout,omitempty"`
	LibrarySharePageLayout   string `json:"librarySharePageLayout,omitempty"`
}
*/

// CatalogResponse is the response from a catalog query
type CatalogResponse struct {
	List []CatalogItem `json:"list,omitempty"`
	// Properties CatalogProperties `json:"properties,omitempty"`
	TimeStamp float64 `json:"ts,omitempty"`
}

// CatalogItem is an item inside a catalog response
type CatalogItem struct {
	AudioSourceCount             float64    `json:"audioSourceCount,omitempty"`
	Authenticated                bool       `json:"authenticated,omitempty"`
	CanEdit                      bool       `json:"canEdit,omitempty"`
	Captions                     bool       `json:"captions,omitempty"`
	Created                      float64    `json:"created,omitempty"`
	Description                  string     `json:"description,omitempty"`
	Duration                     float64    `json:"duration,omitempty"`
	Hidden                       bool       `json:"hidden,omitempty"`
	Images                       DataImages `json:"images,omitempty"`
	MediaProcessingError         string     `json:"mediaProcessingError,omitempty"`
	MediaProcessingStatus        string     `json:"mediaProcessingStatus,omitempty"`
	MediaProcessingTransactionID string     `json:"mediaProcessingTransactionId,omitempty"`
	Modified                     float64    `json:"modified,omitempty"`
	OwnerName                    string     `json:"ownerName,omitempty"`
	PermLink                     string     `json:"permLink,omitempty"`
	// Properties                   []interface{} `json:"properties,omitempty"`
	Scope            string   `json:"scope,omitempty"`
	Title            string   `json:"title,omitempty"`
	Type             string   `json:"type,omitempty"`
	UserID           string   `json:"userId,omitempty"`
	UUID             string   `json:"uuid,omitempty"`
	VideoSourceCount float64  `json:"videoSourceCount,omitempty"`
	VideoSources     struct{} `json:"videoSources,omitempty"`
}

/*
This mutates, dunno how to handle that yet
type CatalogProperties struct {
	OverrideAssetCoverImages struct {
		Data string `json:"data,omitempty"`
	} `json:"override_asset_cover_images,omitempty"`
	SortOrder struct {
		Data string `json:"data,omitempty"`
	} `json:"sort_order,omitempty"`
}
*/

// DataImages is the image construct inside of a data query
type DataImages struct {
	Large     string `json:"large,omitempty"`
	MediaView string `json:"media_view,omitempty"`
	Small     string `json:"small,omitempty"`
	Square    string `json:"square,omitempty"`
	Thumbnail string `json:"thumbnail,omitempty"`
}

// Catalog interacts with the catalog endpoint
func (svc *DataServiceOp) Catalog(ctx context.Context, cid string, oid string) (*CatalogResponse, error) {
	path := fmt.Sprintf("%v/api/catalog/c/%v/o/%v", svc.client.BaseURL, cid, oid)
	req := mustNewRequest(http.MethodGet, path, nil)

	var item CatalogResponse
	err := svc.client.sendRequest(req, &item)
	if err != nil {
		return nil, err
	}
	return &item, nil
}

// Describe interacts with the describe endpoint
func (svc *DataServiceOp) Describe(ctx context.Context, cid string, oid string) (*DescribeResponse, error) {
	path := fmt.Sprintf("%v/api/describe/c/%v/o/%v", svc.client.BaseURL, cid, oid)
	req := mustNewRequest(http.MethodGet, path, nil)

	var item DescribeResponse
	err := svc.client.sendRequest(req, &item)
	if err != nil {
		return nil, err
	}
	return &item, nil
}
