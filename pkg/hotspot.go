package warpwire

import (
	"context"
	"fmt"
	"time"
)

// ActivityResponse returns the response for an Activity query
type ActivityResponse map[string]struct {
	Title    string                  `json:"title"`
	Duration int                     `json:"duraiton"`
	Creation *time.Time              `json:"creation"`
	URL      string                  `json:"url"`
	Library  string                  `json:"library"`
	Viewers  map[string]ActivityView `json:"viewers"`
}

// ActivityView is an individual view statistic
type ActivityView struct {
	Watched string `json:"watched,omitempty"`
	// Percentage string `json:"percentage,omitempty"` // This appears either as an empty string or an int...just gonna ignore for now
	Completed bool   `json:"completed,omitempty"`
	Start     string `json:"start,omitempty"`
	End       string `json:"end,omitempty"`
	/* Do we want to expose first and lastname? Leaving it out for now...
	FirstName  string `json:"firstName,omitempty"`
	LastName   string `json:"lastName,omitempty"`
	*/
}

// HotspotService is the interface for interacting with the Hotspot endpoing
type HotspotService interface {
	UsageWithLibrary(context.Context, string) (*ActivityResponse, error)
}

// HotspotServiceOp is the operator for the HotspotService
type HotspotServiceOp struct {
	client *Client
}

// UsageWithLibrary returns usage given a library id
func (svc *HotspotServiceOp) UsageWithLibrary(ctx context.Context, id string) (*ActivityResponse, error) {
	path := fmt.Sprintf("%v/api/usage/activity/code/%v", svc.client.BaseURL, id)

	req := mustNewGetRequest(path)

	var item ActivityResponse
	err := svc.client.sendRequest(req, &item)
	if err != nil {
		return nil, err
	}
	return &item, nil
}
