package warpwire

import (
	"context"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestDataCatalog(t *testing.T) {
	got, err := client.Data.Catalog(context.TODO(), "some-collection", "some-asset")
	require.NoError(t, err)
	require.NotNil(t, got)
}

func TestDataDescribe(t *testing.T) {
	got, err := client.Data.Describe(context.TODO(), "some-collection", "some-asset")
	require.NoError(t, err)
	require.NotNil(t, got)
}
