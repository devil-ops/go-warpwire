package warpwire

import (
	"context"
	"fmt"
	"net/url"
)

// LinkInfo is information...about a link
type LinkInfo struct {
	Properties LinkInfoProperties `json:"properties,omitempty"`
	Type       string             `json:"type,omitempty"`
}

// LinkInfoProperties holds the properties for a link
type LinkInfoProperties struct {
	AssetUUID        string `json:"assetUuid,omitempty"`
	MediaLibraryUUID string `json:"mediaLibraryUuid,omitempty"`
}

// LinkService is the interface for interacting with the /link/ endpoint
type LinkService interface {
	UUID(context.Context, string) (*LinkInfo, error)
}

// LinkServiceOp is the operator for the LinkService
type LinkServiceOp struct {
	client *Client
}

// UUID returns UUID information from a given url string
func (svc *LinkServiceOp) UUID(ctx context.Context, urls string) (*LinkInfo, error) {
	path := fmt.Sprintf("%v/api/link/code/%v/", svc.client.BaseURL, url.PathEscape(urls))
	req := mustNewGetRequest(path)

	var item LinkInfo
	err := svc.client.sendRequest(req, &item)
	if err != nil {
		return nil, err
	}
	return &item, nil
}
