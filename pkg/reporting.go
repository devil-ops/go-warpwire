package warpwire

import (
	"context"
	"encoding/json"
	"fmt"
	"sort"
	"strconv"
	"time"
)

// RawStatsResponse is a big ol' map
type RawStatsResponse map[string]interface{}

// DayStat represents a single statistic for a day
type DayStat struct {
	Day     *time.Time
	Viewers int
	Seconds int
}

// StatsResponse it the translated real Stats from a raw response
type StatsResponse struct {
	Start *time.Time
	End   *time.Time
	Stats []DayStat
}

func mustParseYMD(s string) time.Time {
	t, err := time.Parse("2006-01-02", s)
	panicIfErr(err)
	return t
}

// UnmarshalJSON uses a custom override to do all the json stuff
func (s *StatsResponse) UnmarshalJSON(b []byte) error {
	var rsr RawStatsResponse
	err := json.Unmarshal(b, &rsr)
	if err != nil {
		return err
	}
	for k, v := range rsr {
		switch k {
		case "start":
			ds := v.(string)
			startD := mustParseYMD(ds)
			s.Start = &startD
		case "end":
			ds := v.(string)
			dsD := mustParseYMD(ds)
			s.End = &dsD
		default:
			dsD := mustParseYMD(k)
			dayS := DayStat{
				Day: &dsD,
			}
			viewInfo := v.(map[string]interface{})

			// Sometimes a string, other times a float 🤷
			viewersS, ok := viewInfo["viewers"].(string)
			if ok {
				dayS.Viewers, err = strconv.Atoi(viewersS)
				if err != nil {
					return err
				}
			} else {
				viewersI := viewInfo["viewers"].(float64)
				dayS.Viewers = int(viewersI)
			}

			// Sometimes a string, other times a float 🤷
			secondsS, ok := viewInfo["seconds"].(string)
			var secondsF float64
			if ok {
				secondsF, err = strconv.ParseFloat(secondsS, 64)
				if err != nil {
					return err
				}
			} else {
				secondsF = viewInfo["seconds"].(float64)
			}
			dayS.Seconds = int(secondsF)
			s.Stats = append(s.Stats, dayS)
		}
	}
	return nil
}

// ReportingService is the interface for the reporting endpoint
type ReportingService interface {
	Views(context.Context, ReportingOpts) (*StatsResponse, error)
	Plays(context.Context, ReportingOpts) (*StatsResponse, error)
}

// ReportingOpts are the options that can be passed to a given reporting endpoint
type ReportingOpts struct {
	Collection string     `url:"-"`
	Asset      string     `url:"-"`
	Start      *time.Time `url:"start,omitempty" layout:"2006-01-02"`
	End        *time.Time `url:"end,omitempty" layout:"2006-01-02"`
}

// ReportingServiceOp is the operator for the ReportingService
type ReportingServiceOp struct {
	client *Client
}

func (svc *ReportingServiceOp) viewsOrPlays(ctx context.Context, opts ReportingOpts, vop string) (*StatsResponse, error) { // nolint:unparam // I know ctx isn't used yet...but it willlll be...
	urlopts := mustQueryValues(opts)

	var item StatsResponse
	err := svc.client.sendRequest(
		mustNewGetRequest(fmt.Sprintf("%v/api/stats/%v/c/%v/o/%v/?%v", svc.client.BaseURL, vop, opts.Collection, opts.Asset, urlopts.Encode())),
		&item,
	)
	if err != nil {
		return nil, err
	}
	// Sorty by date
	sort.Slice(item.Stats, func(i, j int) bool {
		return item.Stats[i].Day.Before(*item.Stats[j].Day)
	})
	return &item, nil
}

// Views returns the statistics for the /views/ report
func (svc *ReportingServiceOp) Views(ctx context.Context, opts ReportingOpts) (*StatsResponse, error) {
	return svc.viewsOrPlays(ctx, opts, "views")
}

// Plays returns the statistics for the /plays/ report
func (svc *ReportingServiceOp) Plays(ctx context.Context, opts ReportingOpts) (*StatsResponse, error) {
	return svc.viewsOrPlays(ctx, opts, "plays")
}
