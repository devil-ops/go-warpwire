/*
Package warpwire is the main sdk for using go with WarpWire
*/
package warpwire

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"time"

	"github.com/google/go-querystring/query"
	"github.com/rs/zerolog/log"
	"moul.io/http2curl"
)

// Client holds the base info used for interactions
type Client struct {
	client    *http.Client
	UserAgent string
	BaseURL   string
	Username  string
	Password  string
	Token     string
	// Service interfaces
	Hotspot   HotspotService
	Reporting ReportingService
	Link      LinkService
	Data      DataService
}

// GetToken returns the token as plain text
func (c *Client) GetToken() (string, error) {
	if c.Token == "" {
		err := c.setToken()
		if err != nil {
			return "", err
		}
	}
	return c.Token, nil
}

// setToken turns a username and password in to a Bearer token
func (c *Client) setToken() error {
	req, err := http.NewRequest("POST", fmt.Sprintf("%s/api/authenticate/", c.BaseURL), nil)
	if err != nil {
		return err
	}
	req.SetBasicAuth(c.Username, c.Password)

	res, err := c.client.Do(req)
	if err != nil {
		return err
	}
	b, _ := io.ReadAll(res.Body)

	defer dclose(res.Body)

	var authn AuthenticationResponse
	if err = json.NewDecoder(bytes.NewReader(b)).Decode(&authn); err != nil {
		return err
	}

	if authn.Status != "success" {
		return errors.New("could not get a token")
	}

	c.Token = authn.Token

	return nil
}

func (c *Client) sendRequest(req *http.Request, v interface{}) error {
	if c.Token == "" {
		err := c.setToken()
		if err != nil {
			return err
		}
	}
	bearer := "Bearer " + c.Token
	req.Header.Add("Authorization", bearer)

	if os.Getenv("CURL_DEBUG") != "" {
		command, _ := http2curl.GetCurlCommand(req)
		fmt.Fprintf(os.Stderr, "%v\n", command)
	}

	res, err := c.client.Do(req)
	req.Close = true
	if err != nil {
		log.Warn().Err(err).Send()
		return err
	}

	defer dclose(res.Body)

	if res.StatusCode < http.StatusOK || res.StatusCode >= http.StatusBadRequest {
		var errRes ErrorResponse
		b, _ := io.ReadAll(res.Body)
		log.Warn().Msg(string(b))
		if err = json.NewDecoder(res.Body).Decode(&errRes); err == nil {
			return errors.New(errRes.Message)
		}

		switch {
		case res.StatusCode == http.StatusTooManyRequests:
			return fmt.Errorf("too many requests.  Check rate limit and make sure the userAgent is set right")
		case res.StatusCode == http.StatusNotFound:
			return fmt.Errorf("that entry was not found, are you sure it exists?")
		default:
			return fmt.Errorf("error, status code: %d", res.StatusCode)
		}
	}

	b, _ := io.ReadAll(res.Body)
	if string(b) != "" {
		if err = json.NewDecoder(bytes.NewReader(b)).Decode(&v); err != nil {
			return err
		}
	} else {
		// When there is no body
		return nil
	}

	return nil
}

// New returns a new WarpWire client
func New(opts ...func(*Client)) *Client {
	c := &Client{
		client:    http.DefaultClient,
		UserAgent: "warpwire-report-go",
		BaseURL:   "https://duke.warpwire.com",
	}
	for _, o := range opts {
		o(c)
	}

	if c.Username == "" {
		panic("must set username")
	}
	if c.Password == "" {
		panic("must set password")
	}

	c.Hotspot = &HotspotServiceOp{client: c}
	c.Reporting = &ReportingServiceOp{client: c}
	c.Link = &LinkServiceOp{client: c}
	c.Data = &DataServiceOp{client: c}

	return c
}

// WithAuthEnv sets username and password from environment variables
func WithAuthEnv() func(*Client) {
	u := os.Getenv("WARPWIRE_USERNAME")
	p := os.Getenv("WARPWIRE_PASSWORD")
	return func(c *Client) {
		if u != "" {
			c.Username = u
		}
		if p != "" {
			c.Password = p
		}
	}
}

// WithBaseURL changes the base url of all the client calls
func WithBaseURL(u string) func(*Client) {
	return func(c *Client) {
		c.BaseURL = u
	}
}

// WithToken sets the auth token. This is mostly just going to be used for testing, we'll usually get this by sending the username/password up.
func WithToken(t string) func(*Client) {
	return func(c *Client) {
		c.Token = t
	}
}

// WithUsername sets the username for authentication
func WithUsername(u string) func(*Client) {
	return func(c *Client) {
		c.Username = u
	}
}

// WithPassword sets the password for authentication
func WithPassword(u string) func(*Client) {
	return func(c *Client) {
		c.Password = u
	}
}

// Response holds information about an http response. Nothing really to see here right now
type Response struct {
	*http.Response
}

// ErrorResponse is for responses that are errors
type ErrorResponse struct {
	Message string `json:"errors"`
}

// AuthenticationResponse is the response from an authentication request
type AuthenticationResponse struct {
	Expiration AuthExpiration `json:"expiration,omitempty"`
	Method     string         `json:"method,omitempty"`
	Status     string         `json:"status,omitempty"`
	Token      string         `json:"token,omitempty"`

	// These are returned on errors only
	Code    float64 `json:"code,omitempty"`
	Message string  `json:"message,omitempty"`
}

// AuthExpiration is a special struct to hold the time, converted from either an int or string
type AuthExpiration struct {
	time.Time
}

// UnmarshalJSON overrides the default unmarshalling
func (a *AuthExpiration) UnmarshalJSON(data []byte) error {
	// Simplified check
	utime := string(bytes.Trim(data, `"`))
	i, err := strconv.ParseInt(utime, 10, 64)
	if err != nil {
		return err
	}
	a.Time = time.Unix(i, 0)
	return nil
}

func mustNewGetRequest(u string) *http.Request {
	return mustNewRequest("GET", u, nil)
}

func mustNewRequest(m, u string, b io.Reader) *http.Request {
	r, err := http.NewRequest(m, u, b)
	if err != nil {
		panic(err)
	}
	return r
}

/*
func assetIDWithURL(u string) (string, error) {
	u = strings.TrimSuffix(u, "/")
	parts := strings.Split(u, "/")
	return parts[len(parts)-1], nil
}

func (c *Client) AssetsWithLibrary(ctx context.Context, id string) ([]string, error) {
	assets := []string{}
	res, err := c.Hotspot.UsageWithLibrary(ctx, id)
	if err != nil {
		return nil, err
	}
	for _, v := range *res {
		aid, err := assetIdWithURL(v.URL)
		if err != nil {
			return nil, err
		}
		assets = append(assets, aid)
	}
	return assets, nil
}
*/

func panicIfErr(err error) {
	if err != nil {
		panic(err)
	}
}

func dclose(c io.Closer) {
	err := c.Close()
	if err != nil {
		log.Warn().Err(err).Msg("error closing item")
	}
}

func mustQueryValues(opts any) url.Values {
	urlopts, err := query.Values(opts)
	if err != nil {
		panic(err)
	}
	return urlopts
}
